# Description

This repository contains an implementation in `C` for embedded micro-controllers of an Extended Kalman Filter for UGV navigation. Measurement used are synchronous UWB ranging message performing PTP synchronisation between agents using TOA protocol. For further details, please read the documentation PDF and the companion article. 

## Documentation

All the documentation is contained inside `application_notes.pdf`at the root of this repository.

## Contributor

Justin Cano, PhD Student, Polytechnique Montréal (QC, Canada) - ISAE Supaéro (Toulouse, France)

Reference
---------------

This implementation comes alongside the following paper published in ICRA 2019
proceedings :

J. Cano, S. Chidami, and Le Ny, Jérôme, “A Kalman Filter-Based Algorithm
for Simultaneous Time Synchronization and Localization in UWB Networks,”
in ICRA, Montreal, QC, Canada, 2019.

Article preprint is available on http://justincano.com