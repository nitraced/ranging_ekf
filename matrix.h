#include <stdint.h>
#include <stdlib.h>
#include "precision_floating.h"

//uint8_t biblio_matrices_all_valid = 1;

// Matrix struct
typedef struct matrix{
	uint16_t n_rows;
	uint16_t n_columns;
	fl* buffer;
} matrix;


// Matrix allocation
matrix createMatrix(uint16_t,uint16_t);
/* Low level functions */
// Coefficient Setter 
void mset(matrix,fl, uint16_t, uint16_t);
// Coefficient getter
fl mget(matrix,uint16_t, uint16_t);
// Fill a matrix with zeros
void mzeros(matrix);

/* Test : product validation */
int prodValidation(matrix, matrix);
/* Unary operations for square matrices */
void mtrsp(matrix,matrix);
// Fill a matrix with zeros
void mzeros(matrix);


/* Binary operations */

/* Copy A <- B */
void mcopy(matrix,matrix);

/* Addition SUM = A + B */
void madd(matrix, matrix, matrix);
/* Substraction SUB = A - B */
void msub(matrix,matrix,matrix);
/* Scalar multiplication RES = A*k */
void mscalmul(matrix, matrix, fl);
/* Identity complement RES = I_n - M */
void midcomp(matrix, matrix);
/* Multiplication MUL = A*B */
void mmul(matrix,matrix, matrix);
/* Sum of elementwise multiplication */
fl mscal(matrix, matrix);
/* Inversion (cholesky) */
void mchol(matrix,matrix);


/* Print matrix */
void mprint(matrix,char);

/* Print matrix for matlab format */
void mprintmatlab(matrix,char);

/* Fill randomly a matrix for debug */
void mrandom(matrix M);
