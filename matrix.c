#include "matrix.h"
#include "stdio.h"
#include "stdint.h"
#include <stdlib.h>
#include <time.h> 
#include <string.h>

// Matrix allocation
matrix createMatrix(uint16_t n_rows,uint16_t n_columns){
	matrix M;
	M.n_rows = n_rows;
	M.n_columns = n_columns;
	M.buffer = malloc(n_rows*n_columns*sizeof(fl));
	return M;
}

/* Low level functions */
// Coefficient Setter 
void mset(matrix M,fl value, uint16_t row, uint16_t column){
	M.buffer[row+M.n_rows*column]=value;
}
// Coefficient getter
fl mget(matrix M,uint16_t row, uint16_t column){
	return M.buffer[row+M.n_rows*column];
}
// Fill a matrix with zeros
void mzeros(matrix M){
	uint16_t i,j;
	for(i=0;i<M.n_rows;i++){
		for(j=0;j<M.n_columns;j++){
			mset(M,0.0,i,j);
		}
	}
}

/* Test : product validation */

int prodValidation(matrix M, matrix G){
	if(M.n_columns == G.n_rows){
			// Valid product
			return 1;
	}
	return 0; // Nongood product
}


/* Unar operations for square matrices */

void mtrsp(matrix T, matrix M){
	uint16_t i,j;
	fl v;
	for(i=0;i<M.n_rows;i++){
		for(j=0;j<M.n_columns;j++){
			v = mget(M,i,j);
			mset(T,v,j,i);
		}
	}
}

/* Binary operation */

/* Copy Copy = Original */
void mcopy(matrix Copy, matrix Original){
	uint16_t i,j;
	for(i=0;i<Original.n_rows;i++){
		for(j=0;j<Original.n_columns;j++){
			mset(Copy,mget(Original,i,j),i,j);
		}
	}
}

/* Addition SUM = A + B */
void madd(matrix SUM, matrix A, matrix B){
	uint16_t i,j;
	fl v;
	for(i=0;i<A.n_rows;i++){
		for(j=0;j<A.n_columns;j++){
			v = mget(A,i,j)+mget(B,i,j);
			mset(SUM,v,i,j);
		}
	}
}

/* Substraction SUB = A - B */
void msub(matrix SUB, matrix A, matrix B){
	uint16_t i,j;
	fl v;
	for(i=0;i<A.n_rows;i++){
		for(j=0;j<A.n_columns;j++){
			v = mget(A,i,j)-mget(B,i,j);
			mset(SUB,v,i,j);
		}
	}
}


/* Scalar multiplication RES = A*k */
void mscalmul(matrix RES, matrix A, fl k){
	uint16_t i,j;
	fl v;
	for(i=0;i<A.n_rows;i++){
		for(j=0;j<A.n_columns;j++){
			v = mget(A,i,j)*k;
			mset(RES,v,i,j);
		}
	}
}

/* Identity complement RES = I_n - M */
void midcomp(matrix RES, matrix M){
	uint16_t i,j;
	fl v;
	for(i=0; i<M.n_rows; i++){
		for(j=0; j<M.n_columns;j++){
			v= -mget(M,i,j);
			if(i==j){
				v+= 1.0; // Substraction from id matrix
			}
			mset(RES,v,i,j);
		}
	}
}

/* Multiplication MUL = A*B */

void mmul(matrix MUL,matrix A, matrix B){
	uint16_t i,j,k,m,n,p;
	fl v;
	m = A.n_rows;
	n = A.n_columns;
	p = B.n_columns;
	/* Sizes A = [m,n] B = [n,p] C = [m,p] */
	for(i=0;i<m;i++){
		for(j=0;j<p;j++){
			v = 0.0;
			for(k=0;k<n;k++){
				v+=mget(A,i,k)*mget(B,k,j);
			}
			mset(MUL,v,i,j);
		}
	}
}

/* Elementwise product sum (scalar product for vectors) */
fl mscal(matrix A, matrix B){
	uint16_t i,j;
	fl v = 0.0;
	for(i=0;i<A.n_rows;i++){
		for(j=0;j<A.n_columns;j++){
			v += mget(A,i,j)*mget(B,i,j);
		}
	}
	return v;
}

/* Cholesky inverse 
 * Caution for sym and definite matrix only
 * */
void mchol(matrix INV,matrix A){
	
	
	// Todo
}

/* Print the matrix in «natural» maths notations */
void mprint(matrix Debug, char name){
	uint16_t ii,jj;
	char str[5];
	str[0]=name;
	str[1]='=';
	str[2]='\n';
	printf(str);
	
	for(ii=0;ii<Debug.n_rows;ii++){
		for(jj=0; jj<Debug.n_columns;jj++){
			printf("%.8f  ",mget(Debug,ii,jj));
		}
		printf("\n");
	}
	printf("\n");
}

/* Print the matrix ready to be entered in matlab/scilab (freeware) CLI or script (usefull to debug ;) */
void mprintmatlab(matrix Debug, char name){
	uint16_t ii,jj;
	printf("%c",name);
	printf("= [");
	for(ii=0;ii<Debug.n_rows;ii++){
		for(jj=0; jj<Debug.n_columns;jj++){
			if(jj<Debug.n_columns-1){
				printf("%.16f ,",mget(Debug,ii,jj));
			}
			else{
				printf("%.16f ",mget(Debug,ii,jj));
			}
		}
		if(ii<Debug.n_rows-1){
			printf(";");
		}
	}
	printf("] \n");
}

// Fill randomly a matrix following the uniform law U(-100,100)
void mrandom(matrix M){
	uint16_t i,j;
	fl r=0.0;
	for(i=0;i<M.n_rows;i++){
		for(j=0;j<M.n_columns;j++){
			// A number folowing U(0,1)
			r = (fl) rand()/RAND_MAX;
			r = 2.0*(r-0.5)*100.0;
			mset(M,r,i,j);
		}	
	}
}
