#include "precision_floating.h"
#include <stdint.h>

// Size of the buffers
#define BUFFER_SIZE (uint32_t)  80*10
#define BASE_STATIONS_NUM 4

typedef struct mes{
	fl rho[BASE_STATIONS_NUM];
	fl gamma;
}mes;

mes measures[BUFFER_SIZE];


struct vehicle{
	// Positionning
	fl x[BUFFER_SIZE];
	fl y[BUFFER_SIZE];
	fl z[BUFFER_SIZE];
	
	fl x_hat[BUFFER_SIZE];
	fl y_hat[BUFFER_SIZE];
	fl z_hat[BUFFER_SIZE];
	
	fl vx[BUFFER_SIZE];
	fl vy[BUFFER_SIZE];
	fl vz[BUFFER_SIZE];
	
	fl vx_hat[BUFFER_SIZE];
	fl vy_hat[BUFFER_SIZE];
	fl vz_hat[BUFFER_SIZE];
	
	fl t[BUFFER_SIZE];
	
	//Synchro
	
	fl gamma[BUFFER_SIZE];
	fl delta[BUFFER_SIZE];
	fl gamma_hat[BUFFER_SIZE];
	fl delta_hat[BUFFER_SIZE];
}vehicle;

struct estimate{
	fl x;
	fl y;
	fl z;
	fl vx;
	fl vy;
	fl vz;
	fl gamma;
	fl delta;
}estimate;
