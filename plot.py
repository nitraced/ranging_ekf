import csv
import numpy as n
import math as m
import matplotlib.pyplot as plt

t = [];
x = [];
y = [];
z = [];
vx = [];
vy = [];
vz = [];
gamma = [];
delta = [];

xh = [];
yh = [];
zh = [];
vxh = [];
vyh = [];
vzh = [];
gammah = [];
deltah = [];

with open('vehicle_data.csv') as f:
	content = f.readlines()
	i = 0
	for line in content: 
		if i>0:
			tab = line.split(",")
			for i in range(len(tab)):
				tab[i] = float(tab[i])
			t.append(tab[0])
			x.append(tab[1])
			vx.append(tab[2])
			y.append(tab[3])
			vy.append(tab[4])
			z.append(tab[5])
			vz.append(tab[6])
			delta.append(tab[7])
			gamma.append(tab[8])
			xh.append(tab[1+8])
			vxh.append(tab[2+8])
			yh.append(tab[3+8])
			vyh.append(tab[4+8])
			zh.append(tab[5+8])
			vzh.append(tab[6+8])
			deltah.append(tab[7+8])
			gammah.append(tab[8+8])
		i=i+1

plt.figure()
plt.title('True positions and estimates')
plt.subplot(3,1,1)
plt.plot(t,xh,'.',label='Estimation')
plt.plot(t,x,'-',label='Ground truth')
plt.legend()
plt.xlabel("time [s]")
plt.ylabel("positons x [m]")

plt.subplot(3,1,2)
plt.plot(t,yh,'.',label='Estimation')
plt.plot(t,y,'-',label='Ground truth')
plt.legend()
plt.xlabel("time [s]")
plt.ylabel("positons y [m]")

plt.subplot(3,1,3)
plt.plot(t,zh,'.',label='Estimation')
plt.plot(t,z,'-',label='Ground truth')
plt.legend()
plt.xlabel("time [s]")
plt.ylabel("positons z [m]")

plt.savefig('res/pos.png')

plt.figure()
plt.title('True positions and estimates')
plt.subplot(3,1,1)
plt.plot(t,vxh,'.',label='Estimation')
plt.plot(t,vx,'-',label='Ground truth')
plt.legend()
plt.xlabel("time [s]")
plt.ylabel("velocities x [m/s]")

plt.subplot(3,1,2)
plt.plot(t,vyh,'.',label='Estimation')
plt.plot(t,vy,'-',label='Ground truth')
plt.legend()
plt.xlabel("time [s]")
plt.ylabel("velocities y [m/s]")

plt.subplot(3,1,3)
plt.plot(t,vzh,'.',label='Estimation')
plt.plot(t,vz,'-',label='Ground truth')
plt.legend()
plt.xlabel("time [s]")
plt.ylabel("velocities z [m/s]")

plt.savefig('res/vel.png')

plt.figure()

plt.subplot(2,1,1)
plt.plot(t,deltah,'.',label='Estimation')
plt.plot(t,delta,'-',label='Ground truth')
plt.legend()
plt.xlabel("time [s]")
plt.ylabel("delta [s]")

plt.subplot(2,1,2)
plt.plot(t,gammah,'.',label='Estimation')
plt.plot(t,gamma,'-',label='Ground truth')
plt.legend()
plt.xlabel("time [s]")
plt.ylabel("gamma [s/s]")

plt.savefig('res/drift.png')

plt.show()



