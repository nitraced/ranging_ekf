function J = jacobian(v,i)
    global posx posy posz speed_of_light 
    d = dist(v,i);
    J = [(v(1)-posx(i))/d 0 (v(3)-posy(i))/d 0 (v(5)-posy(i))/d 0 speed_of_light 0];
end

