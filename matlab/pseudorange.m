function rho = pseudorange(v,i)
   global speed_of_light 
    drift = v(7);
    distance = dist(v,i);
    rho = drift*speed_of_light + distance;
end

