%% Out of C kalman (to test)

%% Load new dataset
close all
%load('sim.mat')
rho = {rho0,rho1,rho2,rho3};
global speed_of_light posx posy posz dt

%% Parameters of the simulation
posx = [2,2,-2,-2];
posy = [-2,2,-2,2];
posz = [0,.5,.5,0];

dt = 1/80;
N = length(rho1);

speed_of_light = 299792458;

% Matrices Q, P and A
sigmar = 0.1;
sigmax = 3; 
sigmay = 3;
sigmaz = 0.2;
sigmad = 1e-8;
sigmag = 1e-6;
 
bA = [1 dt; 0 1];
z2 = zeros(2);
A = [bA z2 z2 z2;
     z2 bA z2 z2;
     z2 z2 bA z2;
     z2 z2 z2 bA];
bQ = [dt^3/3 dt^2/2;
      dt^2/2 dt];
Q = [bQ*sigmax^2 z2 z2 z2;
    z2 bQ*sigmay^2 z2 z2;
    z2 z2 bQ*sigmaz^2 z2;
    z2 z2 z2 bQ*sigmag^2 + [1 0; 0 0]*sigmad^2]

P = 10*Q + [z2 z2 z2 z2;
         z2 z2 z2 z2;
         z2 z2 z2 z2;
         z2 z2 z2 diag([1e-2,1])];
X = [0 0 0 0 0 0 0 0]';    
% Simulation loop
station_id = 1;
% We stack vector
x_vec = zeros(8,N);
inno_vec = zeros(N,1);
for i=1:N
    % Prediction
    X_pr = A*X;
    P_pr = A*P*A'+Q;
    % Measurement update and projection in measure space
    J = jacobian(X_pr,station_id);
    mes = rho{station_id}(i);
    pred = pseudorange(X_pr,station_id);
    % Innovation
    inno = mes - pred;
    inno_vec(i) = inno;
    S = J*P_pr*J'+sigmar*sigmar;
    % Kalman gain computation
    K = P_pr*J'/S;
    % Updated steps
    X = X_pr + K*inno;
    P = (eye(8)-K*J)*P_pr;
    % Stack estimates
    x_vec(:,i)=X;
    if station_id == 4
        station_id = 1;
    else
       station_id = station_id + 1; 
    end
end

figure()
x_true = [x'; vx'; y'; vy'; z'; vz'; delta';gamma1']; 
for i=1:8   
    subplot(2,4,i)
    plot(x_vec(i,:))
    grid on
    hold on
    plot(x_true(i,:))
    title(num2str(i));
end


figure()
for i=1:8   
    subplot(2,4,i)
    semilogy(abs(x_vec(i,:)-x_true(i,:)))
    grid on
    hold on
    title(num2str(i));
end

figure()
plot(x_true(1,:),x_true(3,:),'k')
hold on
grid on
plot(x_vec(1,:),x_vec(3,:),'.')

