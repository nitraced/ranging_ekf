function d=dist(v,i)
   global posx posy posz
   posBalise = [posx(i); posy(i); posz(i)];
   posEst = [v(1); v(3); v(5)];
   d = norm(posBalise-posEst);
end