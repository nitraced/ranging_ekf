#include "kalman_localization.h"
#include "simulation_data.h"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h> //Ne pas oublier d'inclure le fichier time.h
#include <string.h>

typedef double fl;
fl time_step = 1.0/80.0;

static const fl tab_posxS[6]={2,2,-2,-2};
static const fl tab_posyS[6]={-2,2,-2,2};
static const fl tab_poszS[6]={0,.5,.5,0};
static const fl cS = 299792458.0;

// Test function prototypes
fl normc(fl);
void generate_dataset();
void simulation();
fl pseudorange(uint32_t,uint32_t);
fl gamma_mes(uint32_t);
void store_vehicle();
void store_measurements();

fl normc(fl sigma){
	// Normal distribution centered and with sigma variance
	fl res,u1,u2;
	u1 = (fl) rand()/(fl) RAND_MAX;
	u2 = (fl) rand()/(fl) RAND_MAX;
	// We use box muller transform to have normal centered and with sigma = 1
	res = sqrt(-2.0*log(u1+1.0e-10))*cos(2*3.141592653589793*u2);
	// We have to scale it to the good variance
	res *= sigma;
	return res;
}

void simulation(){
	uint32_t i,j;
	// Simulated measurements
	// We track object with a slight circular velocity
	fl r = 1.0; // Radius of the circular trajectory
	fl omega = 2*3.14/10.0; // Approx 1 turn per 10 seconds
	// Initial conditions
	// Skew is constant (-2 ppm)
    vehicle.gamma[0]= -2.0e-6;
    // Initial offset is equal to 10 ms
    vehicle.delta[0]= 10e-3;
    vehicle.t[0]=0.0;
	/* Simulation of the true states */
	for(i=0;i<BUFFER_SIZE;i++){
		if(i>0){
		// Recursive generations
		// Assuming a true constant skew
		vehicle.gamma[i]=vehicle.gamma[i-1];
		vehicle.delta[i]=vehicle.delta[i-1]+vehicle.gamma[i-1]*time_step;
		// Time in the robot
		vehicle.t[i]=vehicle.t[i-1]+time_step;
		}
		// Trajectories
		// Velocities
		vehicle.vx[i] = -r*omega*sin(omega*vehicle.t[i]);
		vehicle.vy[i] = r*omega*cos(omega*vehicle.t[i]);
		vehicle.vz[i] = 0.0; // It's a robot
		// Positions
		vehicle.x[i] = r*cos(omega*vehicle.t[i]);
		vehicle.y[i] = r*sin(omega*vehicle.t[i]);
		vehicle.z[i] = 0.0; // It's a robot
		/* Measurements */
		measures[i].gamma = gamma_mes(i)+normc(1.0e-7);
		// For each base stations
		for(j=0;j<4;j++){
			measures[i].rho[j]=pseudorange(i,j);
		}
	}
}

fl pseudorange(uint32_t time_indice,uint32_t station){
	fl rho,q;
	rho = 0.0;
	
	// Compute the euclidian distance
	q = (vehicle.x[time_indice]-tab_posxS[station]);
	q *=q;
	rho += q;
	
	q = (vehicle.y[time_indice]-tab_posyS[station]);
	q *=q;
	rho += q;
	
	q = (vehicle.z[time_indice]-tab_poszS[station]);
	q *=q;
	rho += q;
	
	rho = sqrt(rho);
	// Add the drift perturbation
	rho += cS*vehicle.delta[time_indice];
	
	// Additive noise
	rho += normc(0.1);
		
	return rho;
}

fl gamma_mes(uint32_t time_indice){
	fl m;
	// Measurement of the skew perturbated
	m = vehicle.gamma[time_indice]+normc(1.0e-7);
	return m;
}

void store_vehicle(){
	FILE *fp;
	uint32_t i;
	char* filename;
	filename = "vehicle_data.csv";
	// Delete old csv
	remove(filename);
	fp=fopen(filename,"w+");
	fprintf(fp,"t,x,vx,y,vy,z,vz,delta,gamma,x_hat,vx_hat,y_hat,vy_hat,z_hat,vz_hat,delta_hat,gamma_hat\n");
	for(i=0;i<BUFFER_SIZE;i++){
		fprintf(fp,"%.16f,%.16f,%.16f,%.16f,%.16f,%.16f,%.16f,%.16f,%.16f,%.16f,%.16f,%.16f,%.16f,%.16f,%.16f,%.16f,%.16f\n", vehicle.t[i], vehicle.x[i],vehicle.vx[i],vehicle.y[i],vehicle.vy[i],vehicle.z[i],vehicle.vz[i],vehicle.delta[i],vehicle.gamma[i],vehicle.x_hat[i],vehicle.vx_hat[i],vehicle.y_hat[i],vehicle.vy_hat[i],vehicle.z_hat[i],vehicle.vz_hat[i],vehicle.delta_hat[i],vehicle.gamma_hat[i]);
	}
}

void store_measurements(){
	FILE *fp;
	uint32_t i;
	char* filename;
	filename = "measurements.csv";
	// Delete old csv
	remove(filename);
	fp=fopen(filename,"w+");
	fprintf(fp,"t,rho0,rho1,rho2,rho3\n");
	for(i=0;i<BUFFER_SIZE;i++){
		fprintf(fp,"%.16f,%.16f,%.16f,%.16f,%.16f\n",vehicle.t[i],measures[i].rho[0],measures[i].rho[1],measures[i].rho[2],measures[i].rho[3]);
	}
}

int main(){
	uint32_t i;
	uint8_t station=0;
	printf("Running_simulations \n");
	simulation();
	printf("Kalman initilization \n");
	ekf_init();
	printf("Kalman filtering \n");

	for(i=0;i<BUFFER_SIZE;i++){

		set_indice_sim(i);

		ekf_loop(station);
		vehicle.x_hat[i] = estimate.x;
		vehicle.y_hat[i] = estimate.y;
		vehicle.z_hat[i] = estimate.z;
		
		vehicle.vx_hat[i] = estimate.vx;
		vehicle.vy_hat[i] = estimate.vy;
		vehicle.vz_hat[i] = estimate.vz;
		
		vehicle.delta_hat[i] = estimate.delta;
		vehicle.gamma_hat[i] = estimate.gamma;
		
		if(station<3){
			station++;
		}
		else{
			station=0;
		}
		//printf("station_id = %d\n",station);
	}
	
	store_vehicle();
	store_measurements();
	printf("Datalog written exit \n");
	return 0;
}
