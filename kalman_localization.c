#include "kalman_localization.h"
#include "simulation_data.h"
#include "matrix.h"
#include <stdint.h>
#include <math.h>

static const fl tab_posx[6]={2,2,-2,-2};
static const fl tab_posy[6]={-2,2,-2,2};
static const fl tab_posz[6]={0,.5,.5,0};
static const fl SPEED_OF_LIGHT = 299792458.0;

/* EKF gain tuning */
// Measurement noises 
#define  nu_rho      (fl) 0.1
#define  nu_gamma    (fl) 1.0e-7
// Process noises PSD
#define  sigma_x     (fl) 4.0
#define  sigma_y     (fl) 4.0
#define  sigma_z     (fl) 0.001
#define  sigma_delta (fl) 1.0e-8
#define  sigma_gamma (fl) 1.0e-6

/* Local shared variables
 *  */

static matrix x,prx,J,A,Q,P,prP,trJ,K,tempVecColumn,tempVecLine,tempSquare;
static fl r, innov, innov_cov, meas, delta_t;
//static uint32_t previous_time;
//static fl A_delta = 0.01;
static fl Q_psd[5]={sigma_x*sigma_x,sigma_y*sigma_y,sigma_z*sigma_z,sigma_gamma*sigma_gamma,sigma_delta*sigma_delta}; 
static fl r_gamma = nu_gamma;
static fl r_rho = nu_rho;

// Private functions prototypes

static void fill_matrices(void);
static fl d(uint8_t i);
static fl y(uint8_t i);
static void jacobian_computation(uint8_t);
static void getMeasure(uint8_t);
static void updateMatrices(uint8_t);

static void fill_matrices(){
	uint16_t i;
	// All matrices are cleared with zeros first
	mzeros(x); 
	mzeros(A);
	mzeros(Q);
	mzeros(P);
	meas = 0.0;
	// Initial stepping time (false)
	delta_t = 1.0/80.0;
	// A = I_8 at first
	for(i=0;i<8;i++){
		mset(A,1.0,i,i);
	}
	// We only have information on initial position and drift
	mset(x,0.0,1-1,0);
	mset(x,0.0,3-1,0);
	mset(x,0.0,5-1,0);
	mset(x,0.0,7-1,0); // A delta/2
	
	// Initialisation for Q and A
	updateMatrices(0);
	mscalmul(P,Q,10.0); // incetitude is strong as first
	// Synch incertitude is strong
	mset(P,1.0e-2,7-1,7-1);
	mset(P,1.0,8-1,8-1);
}

static fl d(uint8_t i){
	fl res, f;
	// x pos
	f = mget(prx,1-1,0) - tab_posx[i];
	res = f*f;
	// y pos
	f = mget(prx,3-1,0) - tab_posy[i];
	res += f*f;
	// z pos
	f = mget(prx,5-1,0) - tab_posz[i];
	res+= f*f;
	// Square root of the sum with a tiny bias to avoid 0 division
	res = sqrt(res)+1.0e-9;
	return res;
}

static fl y(uint8_t i){
	fl res;
	if(i==255){
		// Skew estimate is the 8th component
		res = mget(prx,8-1,0);
	}
	else
	{
		// PSEUDORANGE ESTIMATE
		// Range estimate
		res = d(i);
		// Adddition of the clock offset
		res += SPEED_OF_LIGHT*mget(prx,7-1,0);
	}
	return res;
}

static void jacobian_computation(uint8_t i){
	uint32_t j;
	fl f,dist;
	if(i==255){
		// Jacobian for the gamma measurement
		// 0 for all, except for the last component
		for(j=0;j<7;j++){
			mset(J,0.0,0,j);
		}
		mset(J,1.0,0,8-1);
	}
	else
	{
		for(j=0;j<4;j++){
			// Odd component of the jacobian are set to zero
			mset(J,0.0,0,2*i+1);
		}
		
		dist = d(i); // We compute predicted distance
		
		f = mget(prx,0,0) - tab_posx[i]; // difference, x axis
		mset(J,f/dist,0,0); // Normalization and storage in the 0 component
		
		f = mget(prx,2,0) - tab_posy[i]; // difference, y axis
		mset(J,f/dist,0,2); // Normalization
		
		f = mget(prx,4,0) - tab_posz[i]; // difference, z axis
		mset(J,f/dist,0,4); // Normalization
		
		mset(J,SPEED_OF_LIGHT,0,6); // The speed of light is added for delta component
	}
}

static uint32_t indice_sim;
void set_indice_sim(uint32_t i){
	indice_sim = i;
}

static void getMeasure(uint8_t i){

	// timestamp and mes
	delta_t = 1.0/80.0;
	if(i<255){
		meas = measures[indice_sim].rho[i];
		//printf("pseudorange = %f",meas);
	}
	else {
		meas = measures[indice_sim].gamma;
	}
	
	//printf("indice_sim = %d, id = %d, meas= %.8f \n",indice_sim,i,meas);
}


static void updateMatrices(uint8_t indice){
	// Set A and Q with respect to the time and r with respect to the measurement
	fl dt_2,dt_3;
	uint32_t i;
	if(indice==255){
		r= r_gamma;
	}
	else{
		r=r_rho;
	}
	
	for(i=0; i<4; i++){
		// Update delta_t in A
		mset(A,delta_t,2*i,2*i+1);
	}	
	// True discretization
	dt_2 = delta_t*delta_t;
	dt_3 = dt_2*delta_t;
	dt_2 /=2.0;
	dt_3 /=3.0;
	for(i=0; i<4; i++){
		mset(Q,Q_psd[i]*dt_3,2*i,2*i);
		mset(Q,Q_psd[i]*dt_2,2*i+1,2*i);
		mset(Q,Q_psd[i]*dt_2,2*i,2*i+1);
		mset(Q,Q_psd[i]*delta_t,2*i+1,2*i+1);
	}
	// Do not forget to add the skew noise
	mset(Q,Q_psd[4]*delta_t,6,6);
}

void ekf_init(){
	// Memory allocation for all the matricial results
	x = createMatrix(8,1);
	prx = createMatrix(8,1);
	A = createMatrix(8,8);
	J = createMatrix(1,8);
	
	trJ = createMatrix(8,1);
	P = createMatrix(8,8);
	prP = createMatrix(8,8);
	
	Q = createMatrix(8,8);

	K = createMatrix(8,1);
	tempVecLine = createMatrix(1,8);
	tempVecColumn = createMatrix(8,1);
	tempSquare = createMatrix(8,8);
	
	// Fill matrices with initial values
	fill_matrices();
}

void ekf_loop(uint8_t i){
	/* A new measurement is received, update all matrices and proceed with Kalman filtering */
	getMeasure(i);
	updateMatrices(i);
	
	/* KALMAN FILTER
	 * Prediction steps */
	// x_pred = AX
	mmul(prx,A,x); 
	
	// P_pred = A P A^t + Q
	mtrsp(tempSquare,A); // -> Ts = A^t (local buffer)
	mmul(prP,P,tempSquare); // -> prP = P A^t (prP used as second temp matrix)
	mmul(tempSquare,A,prP); // -> T_s = A P A^t (Temp square reused)
	madd(prP,tempSquare,Q); // -> pr_P = A P A^t + Q
	
	/* Innovation step */
	// innov = mes - mes(x)
	innov = meas - y(i);
	// Jacobian computation
	jacobian_computation(i);
	
	// Innovation covariance
	// S = J prP J^t + r
	mmul(tempVecLine,J,prP); // temp -> J prP (line vec)
	innov_cov = mscal(tempVecLine,J);// S = J prP J^t = J.J prP
	
	/* Kalman gain computation */
	// K = pr P J^t / S S is a scalar
	mtrsp(trJ,J); // -> trJ = J^t
	mmul(tempVecColumn,prP,trJ); // -> K = prP trJ
	mscalmul(K,tempVecColumn,1.0/innov_cov); // K = pr P J^t/S
	/* Prediction steps */
	// x = prx + K*innov
	mscalmul(tempVecColumn,K,innov); // -> temp = K*innov
	madd(x,prx,tempVecColumn); // -> x = prx + K*innov
	
	// P = (I-KJ)prP
	mmul(P,K,J); // -> P = KJ
	midcomp(tempSquare,P); // -> TS = I - KJ
	mmul(P,tempSquare,prP); // -> P = (I-KJ)prP

	// Only for debug
	estimate.x = mget(x,0,0);
	estimate.vx= mget(x,1,0);
	
	estimate.y = mget(x,2,0);
	estimate.vy = mget(x,3,0);
	
	estimate.z = mget(x,4,0);
	estimate.vz = mget(x,5,0);
	
	estimate.delta = mget(x,6,0);
	estimate.gamma = mget(x,7,0);
	
#define PRINT_MATRICES 0
	#if PRINT_MATRICES
	mprintmatlab(A,'A');
	mprintmatlab(prx,'v');
	mprintmatlab(prP,'V');
	mprintmatlab(Q,'Q');
	printf("innov = %.5f \n",innov);
	printf("innov_cov = %.5f \n",innov_cov);
	printf("r = %.5f \n",r);
	mprintmatlab(J,'J');
	mprintmatlab(K,'K');
	mprintmatlab(x,'x');
	mprintmatlab(P,'P');
	#endif

}
